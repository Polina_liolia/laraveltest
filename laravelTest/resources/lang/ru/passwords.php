<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать как минимум 6 символов и совпадать с ранее введенным значением',
    'reset' => 'Ваш пароль изменен!',
    'sent' => 'Мы выслали вам ссылку для изменения пароля!',
    'token' => 'Эта ссылка для изменения пароля недействительна.',
    'user' => "Пользователь с таким e-mail не найден.",
];
